Hajar El Kasmi

# SAE BASH : Installation d'un système d'exploitation

## En quoi consiste ce projet ? 
Ce projet consite à procéder à l'installation d'un autre système d'exploitation (Linux) sur nos machines.
Nous étions libre de choisir le type d'instalation, par **Dual Boot**, **Virtual Box** ou autre.
A la fin, le nouvel OS sera un nouvel environement de travail pour effectuer nos travaux. 

### Le choix du type d'installation
Personnellement j'ai opté pour un Dual Boot, selon moir c'est la solution la plus pratique. 
Le Dual boot me permetra de faire cohabiter les deux OS Linux et Windows sous mon PC.
Je pourrais choisir sous quels OS je souhaite travailler selon mes besoins.
d

#### Pré-requis pour l'installation 

1. Un ordinateur 
2. Une clé USB
3. Un disque Dur externe
4. Une connexion internet 


#### Les grandes étapes à suivre : 

- Nettoyer mon pc ( supprimer les fichiers temporaires ou inutiles)
- Tout enregistrer sur un disque Dur externe 
- Installer Linux + liberer de l'espace pour l'OS.
- Créer une clé bootable + téléchargement de Rufus 
- Démarrer le PC sur la clé pour installer Linux 
- Configurer Linux et l’installer en Dual Boot 
- Redémarrer et retirer la clé Usb. 

##### 1. Nettoyer mon PC

Afin que l'ordinateur soit prêt à recevoir un nouvelle OS, **il faut le nettoyer, supprimer les fichiers temporaires et enregistrer.**

> Aller dans paramètres, stockages, fichiers temporaires , et supprimer les fichiers.
Brancher le Disque Dur et enregistrer tous les Dossiers et Fichiers de votre PC dessus. 
(Cela peut prendre un peu de temps).

##### 2. Installer Linux

Aller sur le lien https://xubuntu.fr/ suivant et télécharger la dernière version de Xubuntu.
Une fois ceci fait, il va falloir libérer de l’espace Disque pour le nouvel OS. 
Pour ce faire : 
Aller dans la gestion des disques :
Faire un clique droit sur le disque local du milieu. ( pour moi c’est ACER (C :) ) 

Faire réduire le volume : on souhaite idéalement libérer 45 Go. 
On procède à un calcul pour savoir combien de giga nous devons retirer 1.024x45 = 46080 MO.
Dans quantité d’espace à réduire  on note  :  46080 Mo.
Cliquez sur réduire puis on voit apparaître 45 go octet non alloué

##### 3. Créer une clé bootable

Avant tout, je conseille de formater sa clé USB.
Entrer la clé usb, faire un clic droit ,cliquer sur formater la clé. 

Pour rendre **une clé USB bootable** on a besoin de logiciel Rufus :
Télécharger Rufus sur le lien suivant : https://rufus.ie/ 

+ Insérer la clé USB et ouvrir Rufus.
+ Rufus détecte automatiquement le support USB inséré.
+ Dans le périphérique on retrouve le  nom de notre clé.
+ Dans type de démarrage sélectionner  ‘image ou ISO’ car nous allons mettre l’image de xubuntu dessus. 
+ Cliquer sur sélectionner puis sélectionner le logiciel Xubuntu précédemment installé qui se trouve dans les Téléchargements.
+ Cliquer sur Ouvrir et sur démarrer.

Un message de garde nous dit : **«  attention toutes les donnée de la clé USB vont être effacé »** : cliquer sur ok.
**(normalement aucun souci, car j’ai au préalable formaté ma clé pour qu’il ne reste plus rien dessus)**.
Une fois le téléchargement fini, redémarrer le PC  en allant dans paramètres -> récupération -> redémarrage avancé.
L'ordinateur redemarre.

##### 4. Configurer les paramètres d'installation de Xubuntu 

Une fois que l’écran de démarrage afficher faire apparaître le BIOS/ UEFI  en appuyant sur  la touche correspondante selon votre ordinateur. En l’occurrence moi c’est suppr.

Le Bios apparaît on choisit de démarrer sur le 1er OS : ** try or install Xubuntu** .
L’ordinateur redémarre en xubuntu, une fenêtre est censée apparaître pour configurer l’installation de Xubuntu.
Procédé à la configuration : 

- Choix de la langue
- Choisir de se connecter ou non au réseau (je ne l’ai pas fait pour ma part)
- Type et langue  de clavier
- Choisir d’installer xubuntu de manière normal
- Installer Xubuntu à côté de Windows 
- Et enfin on clique sur installer.
- On entre notre localisation 
- On entre nos informations : nom de l’ordinateur , mot de passe.

On clique sur continuer et on attend le téléchargement de Xubuntu.

- Une fois le téléchargement fini, le PC nous demande de redémarrer, on le redémarre.
- L’ordinateur nous demande de retirer la clé USB et de cliquer sur entrer.
- L’ordinateur redémarre et enfin une fois le redémarrage terminé on peut choisir sous quels OS nous voulons démarrer le PC. 
- L’installation est terminée. 

##### 5. Le Problème  le mode RST

Il se peut que lors du **paramétrage de l’installation de xubuntu un message d’erreur** apparaît : 

![erreur](erreur.jpg)

> Si ce message apparaît : suivre attentivement la procédure suggérée par la documentation officielle d'Ubuntu sous **peine de casser ou endommagé Windows**.

- Le but est de **désactiver le mode RST** et **d’activer le mode AHCI** en toute sécurité.

- Redémarrer l’ordinateur sous Windows
- Aller dans l’Éditeur de Registre (tapez Éditeur dans la barre de recherche du PC).
- Une fois dans l’éditeur tapé les trois lignes surlignée en jaune ci-dessous.
- Et changer les valeurs des clés par les valeurs indiquées:

![éditeur](éditeur.jpg.jpg)

- **Clé Start en 0 – clé 0 en 0 – la clé Start en 0.** 

Fermer l’éditeur une fois ceci fait et ouvrir l’invite de commande.
**Taper la commande suivante afin d’activer le mode sans échecs sur l’orienteur.**

![safeboot](safeboot.jpg.jpg)

Ce mode va permettre à l’ordinateur de **redémarrer de manière plus sécurisée en évitant les messages d’erreur ou autres problèmes de démarrage.**

- On redémarre le PC et on fait apparaître le BIOS.
- Aller dans le Main du Bios faire contrôle S . 
- Aller dans **SATA Mode** et changer le **« Optan Without RAID » en « AHCI ».**

Une fois ceci fait, l'ordinateur nous demande de valider et de redémarrer.
Le problème est normalement réglé et nous pouvons continuer notre installation de Xubuntu en toute tranquillité.
> **Il ne faut pas oublier de retirer le mode sans échec**

###### Enlever le mode sans echec

- Démmarer l'ordinateur sous Windows
- Faire un clic droit sur l’écran de démarrage.
- Cliquez sur Exécuter, une fenêtre va s’ouvrir, taper ‘msconfig’ et entrer.
- Une fenêtre va s’ouvrir, il va falloir décocher dans « options » « mode de démarrage sécurisé ».

Cliquez sur OK on redémarre le PC. 

##### 6. Installation de Vscode sur Xubuntu  :

Aller sur ce lien et télécharger Vscode https://code.visualstudio.com/download, et assurez vous de
**télécharger la version Ubuntu - Debian (.deb)**.

Une fois le téléchargement terminé aller dans le terminal :
```
$ cd Téléchargement
$ ls Téléchargement
```

On voit qu’il y a bien un paquet Debian qui s’est téléchargé.
Puis pour installer le paquet utiliser la commande : 
```
$ sudo dpkg -install (avec le nom du paquets
téléchargé).
```

- Taper le mot de passe afin de s’identifier étant donné que nous avons effectué cette commande en tant que sudo.
- Taper **"code"** dans le Terminal pour ouvrir Vscode.


###### Rendre executable un fichier java 

On tape javac dans le prompt 
Il nous dit que la commande n’existe pas mais peut etre insgtaller si on installe le paquet default-jdk
On installer  default-jdk avec la commande sudo apt-get install default-jdk 
On attend l’instalation.
On va dans notre dossier Executable ou ce trouve notre fichier executable.java avec le programme « helo word »
On tape la commande $ javac Executable
Ce qui crée un fichier executable.class dans notre dossier 
Et enfin on fait $ java  Executable
Ce qui nous renvoi « hello word »

###### Installer git lab : on tape la commande :

```
sudo apt install git-all
```
normalement cette commande suiffit pour tout installer.






